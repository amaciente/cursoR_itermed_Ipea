---
title: "Aula 5 - Dplyer"
author: "Aguinaldo Maciente (phD em R)"
date: "5 de outubro de 2017"
output: word_document
bibliography: Referencias.bib
csl: associacao-brasileira-de-normas-tecnicas-ipea.csl
---


```{r}
#o shortkey para abrir um chunk de código r é Alt+Ctrl+i

data(iris)
iris %>%
  
  
```

Função pra criar colunas
```{r}
library(dplyr)
#Ctrl+Shift
iris %>%
  dplyr::mutate(glob_sd= sd(Petal.Width)) %>%
  dplyr::group_by(Species) %>%
  dplyr::mutate(species_sd= sd(Petal.Width)) %>%
  dplyr::summarise(glob_value= unique(glob_sd),
                   species_value= unique(species_sd)
                   )
knitr::kable

install.packages("nycflights13")

library(nycflights13)
help(flights)
data(flights)
nrow(flights)
```