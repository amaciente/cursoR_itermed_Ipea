---
title: "Aula 6"
author: "Aguinaldo"
output: word_document
---

```{r, eval=F,echo=F,warning=F, message=F}
install.packages("dplyr")
install.packages("nycflights13")
```


```{r,eval=T,echo=F,warning=F, message=F}

library(nycflights13)
library(dplyr)
library(knitr)
library(tidyr)

data(flights)

#names(flights)

flights %>%
  select(tailnum, dest) %>%
  group_by(tailnum) %>%
  summarise(ndest = n_distinct(dest)) %>%
  filter(ndest == 1) %>%
  head(n = 3) %>%
  kable()

total <- flights %>%
  filter(!is.na(dep_time) & dep_time > arr_time) %>%
  summarise(n_flights = n())

```

O total de voos é `r total`.

# Começando a trabalhar com o *gather*.


```{r,echo=F,warning=F, message=F}
kable(head(mtcars,5), caption = "Tabela mtcars", align = 'r')

#Outra forma de fazer

# a função gather transpõe várias variáveis em uma só
# a função spread trasnpõe uma coluna em várias, usando outra como referência
mtcars %>%
  tidyr::gather("peca", "n", cyl, gear, carb) %>%
  dplyr::group_by(peca) %>%
  dplyr::summarise(n_distintos = n_distinct(n)) %>%
  tidyr::spread(peca,n_distintos) %>%
  knitr::kable()
  
```
Agora a base de dados **starwars**.

Mais um exemplo de como criar painéis.

```{r,echo=F,warning=F, message=F}
library(dplyr)
data(starwars)

starwars %>%
  tidyr::gather("tipo", "cor",
                dplyr::ends_with("color")) %>%
  dplyr::group_by(tipo) %>%
  summarise(cores_distintas = n_distinct(cor)) %>%
  kable()

starwars %>%
  dplyr::filter(gender %in% c("male", "female")) %>%
  tidyr::spread(gender,height) %>%
  select(male,female) %>%
  head() %>%
  kable()

```
#Exemplo de base usando nycflights

```{r,echo=F,warning=F, message=F}
flights %>%
  tidyr::unite("trajetos", origin, dest, sep = " - ") %>%
  dplyr::group_by(carrier) %>%
  dplyr::summarise(trajetos_distintos = n_distinct(trajetos)) %>%
  head() %>%
  kable()

install.packages("stringr")
```




